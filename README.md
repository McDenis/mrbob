# MrBob

## _I'm a Discord Bot_

Qui est MrBob? 
MrBob est un bot que j'ai développé à mon temps perdu. 
Pour l'instant, je dirais qu'il est en version "stable" car il est encore en développement, mais quelques commandes sont déjà accessibles.

---
## Listes des commandes

| Commandes | Raccourcis CMD | Paramètres | Commentaire |
| ------ | ------ | ------ | ------ |
| !aide | Aucun | Pas de paramètre | Commande principale qui permet de voir les autres commandes et leur fonctionnement | 
| !profil | !p | [ USER ] | Voir son profil ou celui d'un utilisateur | 
| !xp | Aucun | [ USER ] | Voir son profil d'expérience ou celui d'un utilisateur | 
| !rank | !r | [ UP ] | Voir son rank qu'on possède ou acheter le niveau suivant | 
| !job | !j | [ UP ] | Voir son métier actuel ou acheter une promotion | 
| !daily | !dg | Pas de paramètre | Réclamer ses Goins journalier | 
| !work | !w | Pas de paramètre | Effectué un travail pour gagner des Goins | 
| !dice | !d | [ MISE ] | Jeu d'hasard pour gagner ou perdre des Goins | 
| !pfc | Aucun | Pas de paramètre | C'est un pierre, feuille, ciseaux basique |
| !give | Aucun | [ MONTANT ] [ USER ] | Permet de donner des Goins à un utilisateur |
| !buy | Aucun | [ COULEUR ] | Permet d'acheter une couleur pour les messages de MrBob | 
| !binouze | !b | Pas de paramètre | Achète une binouze avec tes Goins |
| !gartic | Aucun | [ CHOIX ] | Affiche les titres (lien cliquable) afin de voir le GIF d'un Gartic Phone |
| !sondage | !s | [ QUESTION;REPONSES ] | Permet de créer un sondage | 
| !idee | Aucun | [ MESSAGE ] | LPermet de proposé des idées d'amélioration | 
| !feedback | !fb | [ MESSAGE ] | Permet de faire un retour sur un crash/bug | 
| !top | Aucun | Pas de paramètre | Affiche le Top 3 des meilleurs joueurs du PFC |
| !streak | Aucun | Pas de paramètre | Permet d'afficher la personne ayant la meilleure streak en cours (streak perso dans le profil) |
| !quote | Aucun | [ ID ] | Quote sur le message précédent ou celui de l'ID donné |
| !quotelist | !ql | [ USER ] | Afficher sa propre quotelist ou celui d'un utilisateur |
| !rage | Aucun | [ ID ] | Rage sur le message précédent ou celui de l'ID donné (ex: azerty => AzErTy) ||
| !update | !up | Pas de paramètre | Affichage du versionning de MrBob | 
| !version | !v | Pas de paramètre | En savoir plus sur ma version |
| !info | Aucun | Pas de paramètre | En savoir plus sur moi et mon créateur |

---
## Toutes les installations nécessaires

```sh
npm install discord.js@13.15.1
npm install mysql
npm install moment
```

```sh
node main.js
```

--- 

## Pour me souvenir

```sh
git add .
git commit -m "un commentaire"
git push origin master
```