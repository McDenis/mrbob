const fs = require('fs');

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function logErreur(error) {
    var d = new Date()
    fs.appendFileSync('./erreurs/erreurs.log', `[${d.toLocaleString()}] - Error : ${error}\n`)
}

function executeSimpleQuery(bobdb, sQuery) {
    bobdb.query(sQuery, (err) => {
        if (err) {  logErreur(err)  } 
    })
}

function getFichiers(dossier) {
    return new Promise((resolve) => {
        fs.readdir(`./${dossier}/`, (err, files) => {
            if (err) { logErreur(err) }        
            resolve(files)
        })
    })
}

function executeResultQuery(bobdb, sQuery) {
    return new Promise((resolve) => {
        bobdb.query(sQuery, (err, result) => {
            if (err) { logErreur(err) } 
            else {
                if (result.length > 0) { resolve(result) }
                else { resolve(false) }
            }
        })
    })
}

function checkUser(bobdb, userID) {
    sQuery = `SELECT * FROM utilisateurs WHERE IDUtilisateur = ${userID};`

    return new Promise((resolve) => {
        bobdb.query(sQuery, (err , result) => {
            if (err) { logErreur(err)} 
            else {
                if (result.length > 0) { resolve(true) }
                else { resolve(false) }
            }
        })
    })
}

function createUser(bobdb, userID, pseudo, hashtag, bot) {
    let nBot 
    if (bot === false) { nBot = 0 } else { nBot = 1 }
    sQuery = `INSERT INTO utilisateurs (IDUtilisateur, Username, Discriminator, Bot) VALUES (${userID}, '${pseudo}', '${hashtag}', ${nBot})`
    bobdb.query(sQuery, (err) => {
        if (err) { logErreur(err) } 
    });

    sQuery =  `INSERT INTO infoutilisateur (IDUtilisateur) VALUES (${userID});`
    bobdb.query(sQuery, (err) => {
        if (err) { logErreur(err) } 
    });
}

function addExperience(bobdb, userID, xpToAdd) {
    sQuery = `SELECT * FROM infoutilisateur WHERE IDUtilisateur = ${userID};`
    
    bobdb.query(sQuery, (err, result) => {
        if (err) { logErreur(err) } 
        else {
            if (result.length > 0) { 
                if (result[0].XP + xpToAdd >= result[0].NextLvl) {
                    sQuery = `UPDATE infoutilisateur SET Level = Level + 1, XP = 0, NextLvl = NextLvl * 2 WHERE IDUtilisateur = ${userID};`
                    
                    bobdb.query(sQuery, (err) => {
                        if (err) { logErreur(err) } 
                    })
                }
                else {
                    sQuery = `UPDATE infoutilisateur SET XP = XP + ${xpToAdd} WHERE IDUtilisateur = ${userID};`
                    
                    bobdb.query(sQuery, (err) => {
                        if (err) { logErreur(err) } 
                    })
                }
            }
        }
    })
}

module.exports = {
    logErreur: logErreur,
    getFichiers: getFichiers,
    executeSimpleQuery: executeSimpleQuery,
    executeResultQuery: executeResultQuery, 
    checkUser: checkUser,
    createUser: createUser,
    addExperience: addExperience,
    capitalizeFirstLetter: capitalizeFirstLetter
};