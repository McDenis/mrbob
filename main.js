const { Client, Intents, MessageEmbed } = require('discord.js')

const mysql = require('mysql')
const auth = require('./auth.json')
const fonctions = require('./functions/functions.js')
const fs = require('fs')

const client = new Client({ 
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_VOICE_STATES]
})

const bobdb = mysql.createConnection({
    host: 'localhost',
    user: 'bob',
    password: 'mrbob',
    database: 'mrbob'
})

client.on('ready', function (evt) {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({ 
        activities: [{
            name: "!aide | !help",
            type: 'COMPETING', // COMPETING - CUSTOM - LISTENING - PLAYING - STREAMING - WATCHING
        }],
        status: 'online'
    })
})

bobdb.connect(function(err) {
    if (err) { fonctions.logErreur(err) } 
    else { console.log('Connected to MrBob database!') }
})

const cats = [ 'spooky', 'snow', 'nono', 'vanille', 'siouxie' ]
const goins = '<:goins:1080160243595628644>'
const price = Math.floor(Math.random() * (10 - 5 + 1)) + 5

// On créer tous les profils pour ne pas avoir de problème sur les commandes
client.on('ready', async (client) => {
    const guild  = client.guilds.cache.get('407470917887852546'); 
    let res = await guild.members.fetch();
    res.forEach((member) => {
        fonctions.checkUser(bobdb, member.user.id)
        .then(result => {
            if (result === false) {
                fonctions.createUser(bobdb, member.user.id, member.user.username, member.user.discriminator, member.user.bot)
                
                console.log(`Création du profil ${member.user.username}`)
            }
        })
        .catch(error => {
            fonctions.logErreur(error)
            return
        })  
    });
})

client.on('messageCreate', async msg => {
    if (msg.author.bot) return;

    if (msg.author.id !== client.user.id && msg.author.id !== '432610292342587392'){
        let color = '0x407676'

        message = msg.toString()

        if (message.substring(0,1) == "!"){
            // On récupère la couleur de l'utilisateur (si achetée)
            const resultColor = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, C.IDCouleur, C.Nom, C.Hexa
            FROM InfoUtilisateur IU
            LEFT JOIN Couleurs C ON C.IDCouleur = IU.IDCouleur 
            WHERE IDUtilisateur = ${msg.author.id};`)
            
            if (resultColor[0].Hexa !== '0x407676') {
                color = resultColor[0].Hexa
            }
            
            const exampleEmbed = new MessageEmbed()
                .setColor(color)
                .setThumbnail(client.user.displayAvatarURL)
            
            /**************************** START COMMANDS ******************************/
            let args = message.toLowerCase().substring(1).split(' ')
            let cmd = args[0] 

            try{
                switch(cmd) {
                    /**************************** COMMANDES TESTS PERSO ******************************/
                    case 'getid':
                        if (msg.author.id === '497087235460694038') {
                            msg.author.send(args[1].replace(/[<>]/g, ""))
                        }
                    break;
                    case 'addgoins':
                        if (msg.author.id === '497087235460694038') {
                            let valArg1 = args[1] 
                            let valArg2 = args[2]
                            let valArg3 = args[3]
                            if (valArg1 === undefined) { return msg.channel.send('!addgoins [ MONTANT ] [ +- ] [ USER ]')}
                            if (valArg2 === undefined) { valArg2 = '+'}
                            if (valArg3 === undefined) { valArg3 = '497087235460694038'} else { valArg3 = valArg3.replace(/[<@!>]/g, "")}
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins ${valArg2} ${valArg1} WHERE IDUtilisateur = ${valArg3}`)
                            msg.react('✅')
                        }
                    break;
                    case 'addgartic':
                        if (msg.author.id === '497087235460694038') {
                            const args = msg.content.slice(10).trim().split(/ +/)
                            const GarticName = args.slice(0, -1).join(' ').replace(/ /g, '_');
                            const GarticLink = args[args.length - 1];

                            fonctions.executeSimpleQuery(bobdb, `INSERT INTO Gartic_Link (Nom, Lien) VALUES ('${GarticName}', '${GarticLink}')`)
                            msg.react('✅')
                        }
                    break
                    case 'exec':
                        if (msg.author.id === '497087235460694038') {
                            fonctions.executeSimpleQuery(bobdb, msg.content.toLowerCase().substring(6))
                            msg.delete()
                        }
                    break
                    case 'reset':
                        if (msg.author.id === '497087235460694038') {
                            const UserIDReset = args[1].replace(/[<@!>]/g, "")
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = 0, Last_Daily = '2024-01-01 09:00:00', LastWork = '2024-01-01 09:00:00', IDUpgrade = 1, IDMetier = 1 WHERE IDUtilisateur = ${UserIDReset}`)
                            msg.delete()
                        }
                    break
                    /****************************************************************************************/

                    /**************************** MrBob AIDE ******************************/
                    case 'aide':
                    case 'help':
                        fonctions.addExperience(bobdb, msg.author.id, 2)

                        const cmdarg = msg.content.replace(/!/g, '')
                        let title = `MrBob est là pour t'aider`
                        let embedTitle = `Commandes`
                        
                        if (cmdarg === 'help') { 
                            title = `MrBob is here to help`
                            embedTitle = `Commands`
                        }

                        const HelpEmbed = exampleEmbed.setTitle(`${title}`)

                        AideP1 = fs.readFileSync(`./messages/${cmdarg}_1.txt`, 'utf8')
                        AideP2 = fs.readFileSync(`./messages/${cmdarg}_2.txt`, 'utf8')
                        AideP3 = fs.readFileSync(`./messages/${cmdarg}_3.txt`, 'utf8')
                            
                            exampleEmbed.addFields(
                                { name: `${embedTitle}`, value: `${AideP1}` },
                                { name: '_ _', value: `${AideP2}` },
                                { name: '_ _', value: `${AideP3}` }
                            )
                            .setDescription('') 
                        // Commandes Administrateur
                        if (msg.author.id === '497087235460694038') {
                            AideAdmin = fs.readFileSync(`./messages/${cmdarg}_admin.txt`, 'utf8')
                        
                            HelpEmbed.addFields({ name: '_ _', value: `${AideAdmin}` })
                        }
                        msg.react('✅')
                        msg.author.send({ embeds: [ HelpEmbed ] })
                    break
                    /****************************************************************************************/

                    /**************************** MrBob PROFILS ******************************/
                    case 'p':
                    case 'profil':
                        let UserIDProfil
                    
                        if (args[1] === undefined || args[1] === null || args[1] === '') { UserIDProfil = msg.author.id } else { UserIDProfil = args[1].replace(/[<@!>]/g, "") }
                        targetUser = client.users.cache.get(UserIDProfil)

                        const resultProfil = await fonctions.executeResultQuery(bobdb, `
                        SELECT IU.*, UP.Emoji AS EmojiUP, UP.Nom AS NomUP, UP.Modifier, UP.Plus, ME.Emoji AS EmojiME, ME.Nom AS NomME, ME.Min, ME.Max
                        FROM InfoUtilisateur IU
                        LEFT JOIN Upgrades UP ON UP.IDUpgrade = IU.IDUpgrade
                        LEFT JOIN Metier ME ON ME.IDMetier = IU.IDMetier
                        WHERE IDUtilisateur = ${UserIDProfil}`)

                        fonctions.addExperience(bobdb, msg.author.id, 2)
                        
                        total  = resultProfil[0].Gagnant + resultProfil[0].Perdant + resultProfil[0].Draw
                        winrate = ((2 * resultProfil[0].Gagnant) + resultProfil[0].Draw) / (2 * total) * 100
                    
                        if (total > 1) { pText = 'parties' } else { pText = 'partie'}
                    
                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setAuthor({ name: `Profil de ${targetUser.username}`, iconURL: `${targetUser.avatarURL()}`, url: '' })
                                .setDescription(`
                                **Portefeuille :** ${resultProfil[0].Goins} ${goins}
                                _ _
                                **Nombre de partie :** ${total} (<:win:1079117243759218699> ${resultProfil[0].Gagnant} | :x: ${resultProfil[0].Perdant} | <:draw:1079118690743750697> ${resultProfil[0].Draw})
                                **Best streak : ** :medal: ${resultProfil[0].Best}
                                **Pourcentage de victoire :** <:trophe:1122295218725728317> ${winrate.toFixed(2)}% 
                                _ _
                                **Dice Rank :** ${resultProfil[0].EmojiUP} ${resultProfil[0].NomUP}
                                **Dice Pourcentage :** ${resultProfil[0].Modifier}%
                                **Dice Plus :** +${resultProfil[0].Plus}
                                _ _
                                **Travail en cours :** ${resultProfil[0].EmojiME} ${resultProfil[0].NomME}
                                **Variation salaire :** ${resultProfil[0].Min} - ${resultProfil[0].Max} ${goins}
                                _ _
                                **Prix de la bière :** ${price} ${goins}
                                **Nombre de bière :** ${resultProfil[0].Binouze} :beer:
                                `)
                            ]
                        })
                    break
                    
                    case 'xp':
                        let UserIDXP = 0
                        if (args[1] === undefined || args[1] === null || args[1] === '') { UserIDXP = msg.author.id } else { UserIDXP = args[1].replace(/[<@!>]/g, "") }

                        const resultxp = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${UserIDXP}`)
                        targetUser = client.users.cache.get(UserIDXP)

                        XPNextLevel = resultxp[0].NextLvl - resultxp[0].XP
                        userProgression = Math.round((resultxp[0].XP * 100 / resultxp[0].NextLvl), 2)

                        emojiProgression = '<:5pc:1106881992273973370>'
                        if (userProgression >= 25 && userProgression < 50) { emojiProgression = '<:25pc:1106881994341761065>' } 
                        else if (userProgression >= 50 && userProgression < 90) { emojiProgression = '<:50pc:1106881995566481568>' }
                        else if (userProgression >= 90) { emojiProgression = '<:90pc:1106881996883509320>' }

                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setDescription(`**Niveau actuel :** <:level:1106875098075508756> ${resultxp[0].Level} 
                        
                                **Expérience :** <:exp:1106876766422835261> ${resultxp[0].XP} / ${resultxp[0].NextLvl}
                                **Nécessaire :** <:xpup:1106879182228701187> ${XPNextLevel}

                                **Progression :** ${emojiProgression} ${userProgression} %
                                `)
                                .setAuthor({ name: `Expérience de ${targetUser.username}`, iconURL: `${targetUser.avatarURL()}`, url: '' })
                            ]
                        })
                    break

                    case 'rank':
                    case 'rankup':
                        let isRankUp = args[1]
                        if (cmd === 'rankup') { isRankUp = 'up' }
                        if (isRankUp === undefined || (isRankUp.toLowerCase() !== 'up' && isRankUp.toLowerCase() !== 'upgrade')) {
                            const resultRank = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, IU.Goins, UP.Nom, UP.Level, UP.Prix, UP.Modifier, UP.Plus, UP.Emoji, UP.IDUpgrade
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Upgrades UP ON IU.IDUpgrade = UP.IDUpgrade 
                            WHERE IDUtilisateur = ${msg.author.id};`)

                            let resultUpdateRank
                            let ligne = ''
                            
                            if (resultRank[0].IDUpgrade === 9) {
                                ligne = `${resultRank[0].Emoji} **${resultRank[0].Nom}** - Maximum atteint !`                           
                            }
                            else {
                                resultUpdateRank = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Upgrades WHERE IDUpgrade = ${resultRank[0].IDUpgrade + 1}`) 
                                ligne = `${resultRank[0].Emoji} **${resultRank[0].Nom}** - Level suivant : **${resultUpdateRank[0].Emoji} ${resultUpdateRank[0].Nom}** pour **${resultUpdateRank[0].Prix}** ${goins}
                                
                                **Bonus** : +${resultUpdateRank[0].Modifier - resultRank[0].Modifier}% d'obtenir **+${resultUpdateRank[0].Plus}** sur le numéro chance` 
                            }

                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`Votre rank actuel est **${resultRank[0].Nom}**

                                    Vous avez **${resultRank[0].Goins}** ${goins} !
    
                                    Le pourcentage/plus du Dice est disponible sur votre profil (**!p**)
                                    ${ligne}
                                    
                                    Achetez un meilleur rank (**!rank up**) ou visitez le shop (**!buy**)`)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                        else if (isRankUp.toLowerCase() === 'up' || isRankUp.toLowerCase() === 'upgrade') {
                            const resultRankUp = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, IU.Goins, UP.Nom, UP.Level, UP.Prix, UP.Modifier, UP.Plus, UP.Emoji, UP.IDUpgrade
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Upgrades UP ON IU.IDUpgrade = UP.IDUpgrade 
                            WHERE IDUtilisateur = ${msg.author.id};`)

                            if (resultRankUp[0].IDUpgrade === 9) { return msg.channel.send(`Vous possédez déjà le meilleur rank`) }

                            const resultUpRank = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Upgrades WHERE IDUpgrade = ${resultRankUp[0].IDUpgrade + 1}`)
                            const userGoins = resultRankUp[0].Goins
                            const badgePrice = resultUpRank[0].Prix
                            const anwserArray = [
                                {
                                    type: 1,
                                    components: [ 
                                        { type: 2, style: 'SECONDARY', custom_id: 'oui', label: 'Oui', },
                                        { type: 2, style: 'SECONDARY', custom_id: 'non', label: 'Non', }
                                    ],
                                },
                            ]

                            const anwser = await msg.channel.send({
                                content: `Êtes-vous sûr de vouloir acheter le rank "**${resultUpRank[0].Emoji} ${resultUpRank[0].Nom}**" ?`,
                                components: anwserArray
                            })

                            const filtre = button => { return button.user.id === msg.author.id }
                            const response = await anwser.awaitMessageComponent({ filter: filtre, componentType: 'BUTTON', max: 1 });

                            if (response.customId === 'oui') {
                                if (userGoins < badgePrice) {
                                    return response.reply(`Vous n'avez pas assez de Goins pour débloquer le niveau suivant...`)
                                }
                                else {
                                    fonctions.addExperience(bobdb, msg.author.id, 10)

                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = ${userGoins - badgePrice}, IDUpgrade = ${resultRankUp[0].IDUpgrade + 1} WHERE IDUtilisateur = ${msg.author.id}`)
                                    return response.reply(`:star2: Vous venez d'atteindre ${resultUpRank[0].Emoji} **${resultUpRank[0].Nom}**`);
                                }
                            } 
                            else if (response.customId === 'non') {
                                return response.reply(`L'achat du rank a été annulé`);
                            } 
                        }
                    break

                    case 'ranks':
                        const resultRankUser = await fonctions.executeResultQuery(bobdb, `SELECT IDUtilisateur, IDUpgrade FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id}`)
                        const resultRankAll = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Upgrades ORDER BY IDUpgrade`)
                        
                        let i = 0
                        let msgRankResult = ''
                        for (const row of resultRankAll) {
                            if (i === 0) { 
                                msgRankResult += `${resultRankAll[i].Emoji}**${resultRankAll[i].Nom}**` 
                            }
                            else {
                                msgRankResult += `${resultRankAll[i].Emoji}**${resultRankAll[i].Nom}** pour **${resultRankAll[i].Prix}**${goins}`
                            }
                            if (resultRankUser[0].IDUpgrade === resultRankAll[i].IDUpgrade) { msgRankResult += ` <- Rank actuel`}
                            
                            msgRankResult += '\n\n'
                            i++
                        }

                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setTitle('Liste des Ranks')
                                .setDescription(msgRankResult)
                            ]
                        })
                    break

                    case 'job':
                    case 'jobup':
                        let isJobUp = args[1]
                        if (cmd === 'jobup') { isJobUp = 'up' }
                        if (isJobUp === undefined || (isJobUp.toLowerCase() !== 'up' && isJobUp.toLowerCase() !== 'upgrade')) {    
                            const resultJob = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, IU.Goins, IU.IDMetier, M.Nom, M.Gain, M.Min, M.Max, M.Prix, M.Emoji
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Metier M ON IU.IDMetier = M.IDMetier
                            WHERE IU.IDUtilisateur = ${msg.author.id};`)

                            let resultUpdateJob
                            let ligne = ''
                            if (resultJob[0].IDMetier === 10) {
                                ligne = `${resultJob[0].Emoji} **${resultJob[0].Nom}** - Meilleur métier atteint !`                           
                            }
                            else {
                                resultUpdateJob = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Metier WHERE IDMetier = ${resultJob[0].IDMetier + 1}`) 
                                ligne = `${resultJob[0].Emoji} **${resultJob[0].Nom}** - Métier suivant : ${resultUpdateJob[0].Emoji} **${resultUpdateJob[0].Nom}** pour **${resultUpdateJob[0].Prix}** ${goins}
                                
                                **Bonus** : +5${goins} sur le salaire`
                            }

                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`Vous travaillez en tant que **${resultJob[0].Nom}**

                                    Vous avez **${resultJob[0].Goins}** ${goins} !
                                    
                                    Le salaire de ce métier évolu entre **${resultJob[0].Min}** et **${resultJob[0].Max}** ${goins} de l'heure
                                    ${ligne}
    
                                    Achetez une promotion (**!job up**) ou visitez le shop (**!buy**)`)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                        else if (isJobUp.toLowerCase() === 'up' || isJobUp.toLowerCase() === 'upgrade') {
                            const resultJobUp = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, IU.Goins, IU.IDMetier, M.Nom, M.Gain, M.Min, M.Max, M.Prix, M.Emoji
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Metier M ON IU.IDMetier = M.IDMetier
                            WHERE IDUtilisateur = ${msg.author.id};`)

                            if (resultJobUp[0].IDMetier === 10) { return msg.channel.send(`Vous possédez déjà le meilleur métier`) }

                            const resultUpJob = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Metier WHERE IDMetier = ${resultJobUp[0].IDMetier + 1}`)
                            const userGoins = resultJobUp[0].Goins
                            const jobPrice = resultUpJob[0].Prix
                            const anwserJobArray = [
                                {
                                    type: 1,
                                    components: [ 
                                        { type: 2, style: 'SECONDARY', custom_id: 'oui', label: 'Oui', },
                                        { type: 2, style: 'SECONDARY', custom_id: 'non', label: 'Non', }
                                    ],
                                },
                            ]

                            const anwserJob = await msg.channel.send({
                                content: `Êtes-vous sûr de vouloir changer de métier vers "${resultUpJob[0].Emoji} **${resultUpJob[0].Nom}**" ?`,
                                components: anwserJobArray
                            })

                            const filtre = button => { return button.user.id === msg.author.id }
                            const responseJob = await anwserJob.awaitMessageComponent({ filter: filtre, componentType: 'BUTTON', max: 1 });

                            if (responseJob.customId === 'oui') {
                                if (userGoins < jobPrice) {
                                    return responseJob.reply(`Vous n'avez pas assez de Goins pour débloquer le métier suivant...`)
                                }
                                else {
                                    fonctions.addExperience(bobdb, msg.author.id, 10)

                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = ${userGoins - jobPrice}, IDMetier = ${resultJobUp[0].IDMetier + 1} WHERE IDUtilisateur = ${msg.author.id}`)
                                    return responseJob.reply(`:star2: Vous venez d'être promu ${resultUpJob[0].Emoji} **${resultUpJob[0].Nom}**`);
                                }
                            } 
                            else if (responseJob.customId === 'non') {
                                return responseJob.reply(`Le changement de métier a été annulé`);
                            }
                        }
                    break

                    case 'jobs':
                        const resultJobUser = await fonctions.executeResultQuery(bobdb, `SELECT IDUtilisateur, IDMetier FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id}`)
                        const resultJobAll = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Metier ORDER BY IDMetier`)
                        
                        let j = 0
                        let msgJobResult = ''
                        for (const row of resultJobAll) {
                            if (j === 0) { 
                                msgJobResult += `${resultJobAll[j].Emoji} **${resultJobAll[j].Nom}**` 
                            }
                            else {
                                msgJobResult += `${resultJobAll[j].Emoji} **${resultJobAll[j].Nom}** pour **${resultJobAll[j].Prix}**${goins}`
                            }
                            if (resultJobUser[0].IDMetier === resultJobAll[j].IDMetier) { msgJobResult += ` <- Job actuel`}
                            
                            msgJobResult += '\n\n'
                            j++
                        }

                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setTitle('Liste des Jobs')
                                .setDescription(msgJobResult)
                            ]
                        })
                    break
                    /****************************************************************************************/

                    /**************************** MrBob GOINS ******************************/
                    case 'dg':
                    case 'daily':
                        const resultDaily = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id};`) 
                        const lastDaily = resultDaily[0].Last_Daily.getTime()
                        const twentyHours = 20 * 60 * 60 * 1000
                        const currentTime = new Date().getTime()
                        const timeRemaining = lastDaily + twentyHours - currentTime
                        const hoursRemaining = Math.floor(timeRemaining / (1000 * 60 * 60))
                        let minutesRemaining = Math.floor((timeRemaining % (1000 * 60 * 60)) / (1000 * 60))

                        if (lastDaily && ((currentTime - lastDaily - 60000) < twentyHours)) {
                            if (hoursRemaining === 0) { 
                                return msg.channel.send(`Prochain !daily dans **${minutesRemaining}** min`)
                            }
                            else {
                                if (minutesRemaining.toString().length === 1) { minutesRemaining = '0' + minutesRemaining }
                                return msg.channel.send(`Prochain !daily dans **${hoursRemaining}h ${minutesRemaining}** min`)
                            }
                        } 
                        else {
                            fonctions.addExperience(bobdb, msg.author.id, 5)

                            nGoins = Math.floor(Math.random() * (300 - 150 + 1)) + 150 // 150 - 350
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins + ${nGoins}, Last_Daily = DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:00') WHERE IDUtilisateur = ${msg.author.id};`)
                            
                            msg.channel.send(`+**${nGoins}**${goins} ajoutés à votre collection de Goins ! (**${resultDaily[0].Goins + nGoins}** total)`)
                        }
                    break
                    
                    case 'wk':
                    case 'work':
                        fonctions.addExperience(bobdb, msg.author.id, 3)
                        
                        const resultWork = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id}`)
                        const lastWork = resultWork[0].LastWork.getTime()
                        const sixHours = 6 * 60 * 60 * 1000
                        const now = new Date().getTime()
                        const tRemaining = lastWork + sixHours - now
                        const hRemaining = Math.floor(tRemaining / (1000 * 60 * 60))
                        let mRemaining = Math.floor((tRemaining % (1000 * 60 * 60)) / (1000 * 60))

                        if (lastWork && ((now - (lastWork - 60000)) < sixHours)) {
                            if (hRemaining === 0) { 
                                msg.channel.send(`Prochain !work dans **${mRemaining}** min`)
                            }
                            else {
                                if (mRemaining.toString().length === 1) { mRemaining = '0' + mRemaining }
                                msg.channel.send(`Prochain !work dans **${hRemaining}h ${mRemaining}** min`)
                            }
                        } 
                        else {
                            fonctions.addExperience(bobdb, msg.author.id, 3)

                            const resultWorking = await fonctions.executeResultQuery(bobdb, `SELECT IDUtilisateur, Goins, LastWork, IU.IDMetier, M.Nom, M.Gain, M.Min, M.Max
                            FROM infoutilisateur IU 
                            LEFT JOIN Metier M ON M.IDMetier = IU.IDMetier
                            WHERE IDUtilisateur = ${msg.author.id};`) 
                            
                            const work = resultWorking[0].Nom
                            const earningRate = Math.floor(Math.random() * 11) + resultWorking[0].Gain

                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins + ${earningRate * 6}, LastWork = DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:00') WHERE IDUtilisateur = ${msg.author.id};`)

                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`Vous avez effectué votre travail de **${work}**

                                    Ce travail était payé **${earningRate}** ${goins} de l'heure
                                    Vous avez travaillé sur une durée de **6h**

                                    Vous avez donc accumulé **${earningRate * 6}** ${goins}

                                    Vous avez maintenant **${resultWorking[0].Goins + earningRate * 6}** ${goins}
                                    `)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                    break

                    case 'd':
                    case 'des':
                    case 'dice':
                        const reglesKeywords = ['regle', 'regles', 'règle', 'règles']

                        const returnChance = await fonctions.executeResultQuery(bobdb, `SELECT NbChance FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id}`)
                        const numChance = returnChance[0].NbChance
                        const sValue = args[1]

                        if ((sValue === 'start') && (numChance === 0)) { 
                            const nChance = Math.floor(Math.random() * (15 - 5 + 1)) + 5
                            
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET NbChance = ${nChance} WHERE IDUtilisateur = ${msg.author.id}`)

                            return msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`Le tirage de votre numéro chance a été effectué

                                    Vous avez obtenue le numéro : **${nChance}** <:luckydice:1195348326166573126>

                                    **!d [ REGLE ]** pour afficher les règles
                                    **!d [ MISE ]** pour jouer
                                    `)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                        else if ((sValue === 'start') && (numChance > 0)) {
                            return msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`Votre numéro chance a déjà été effectué

                                    Vous avez le numéro : **${numChance}** <:luckydice:1195348326166573126>

                                    **!d REGLE ** pour afficher les règles
                                    **!d MISE ** pour jouer
                                    `)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                        else if (reglesKeywords.includes(sValue)) {
                            return msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`__Voici les règles du dès :__

                                    - Commencer par choisir votre numéro chance avec **!d start** 
                                    - Lancer le dès de 20 faces <:d20:1195348322890817616> avec une mise **!d [MISE]**

                                    - Si le nombre obtenu est inférieur ou égal vous avez gagné sinon vous perdez
                                    - Le 1 est une réussite critique, il multiplie la mise par 3
                                    - Le 20 est un échec critique, vous perdez votre mise plus ¼ de celle-ci

                                    - Augmenter les chances d'obtenir +1 ou +2 sur votre numéro chance avec **!rank**
                                    `)
                                    .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                ]
                            })
                        }
                        else if (numChance === 0) {
                            return msg.channel.send({
                                embeds: [
                                    exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                    .setDescription(`Vous n'avez pas de numéro chance faite **!d start**`)
                                ]
                            })
                        }
                        else {
                            const amount = parseInt(args[1])

                            if (isNaN(amount) || amount < 20) {
                                return msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`Veuillez spécifier un montant de pari valide, une mise de **20** minimum !`)
                                    ]
                                })
                            }

                            fonctions.addExperience(bobdb, msg.author.id, 3)

                            const returnDes = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, IU.Goins, IU.NbChance, UP.Nom, UP.Level, UP.Prix, UP.Modifier, UP.Emoji, UP.IDUpgrade, UP.Plus
                                FROM InfoUtilisateur IU 
                                LEFT JOIN Upgrades UP ON IU.IDUpgrade = UP.IDUpgrade 
                                WHERE IDUtilisateur = ${msg.author.id}`
                            )

                            nMultiplicateur = 1
                            sTexte = ''
                            nPlus = 0
                            nGains = 0

                            const userBalance = returnDes[0].Goins
                            const userChance = returnDes[0].NbChance
                            const userModifier = returnDes[0].Modifier
                            const nbDice = Math.floor(Math.random() * (20 - 1 + 1)) + 1
                            const nbModifier = Math.random();

                            if (userBalance < amount) {
                                return msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: `Portefeuille vide...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`Vous n'avez pas suffisamment de Goins pour effectuer ce pari`)
                                    ]
                                })
                            }

                            if (nbModifier < userModifier) { nPlus = returnDes[0].Plus }

                            if (nbDice <= (userChance + nPlus)) {
                                if (nbDice === 1) { nMultiplicateur = 3 ; sTexte = `**Réussite critique !**` } else { nMultiplicateur = 0.5 ; sTexte = `**Réussite !**`}
                                nGains = Math.round(amount * nMultiplicateur)
                                sTexte += `
                                
                                Numéro chance : **${userChance}** <:luckydice:1195348326166573126> avec bonus **+${nPlus}**
                                Tirage effectué : **${nbDice}**/**20** <:d20:1195348322890817616>

                                Vous avez gagné **${amount + nGains}** ${goins}

                                Votre solde est de **${userBalance + nGains}** ${goins}
                                
                                **!rank** pour voir votre rang`

                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = ${userBalance + nGains} WHERE IDUtilisateur = ${msg.author.id}`)
                                
                                return msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setDescription(`${sTexte}`)
                                        .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                    ]
                                })
                            }
                            else {
                                if (nbDice === 20) { nMultiplicateur = 0.25 ; sTexte = `**Échec critique !**` } else { sTexte = `**Échec !**`}
                                sLigne = ""
                                nGains = Math.round(amount * nMultiplicateur)

                                if (userBalance < (userBalance - nGains)) { 
                                    sLigne = `**0** ${goins}`
                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = 0 WHERE IDUtilisateur = ${msg.author.id}`)
                                }
                                else {
                                    sLigne = `**${userBalance - nGains}** ${goins}`
                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = ${userBalance - nGains} WHERE IDUtilisateur = ${msg.author.id}`)
                                }

                                sTexte += `
                                
                                Numéro chance : **${userChance}** <:luckydice:1195348326166573126> avec bonus **+${nPlus}**
                                Tirage effectué : **${nbDice}**/**20** <:d20:1195348322890817616>

                                Vous avez perdu **${nGains}** ${goins}

                                Votre solde est de ${sLigne}
                                
                                **!rank** pour voir votre rang`
                                
                                return msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setDescription(`${sTexte}`)
                                        .setFooter({ text: `${msg.author.username}`, iconURL: msg.author.displayAvatarURL() })
                                    ]
                                })
                            }
                        }
                    break

                    case 'pfc':
                        const choices = ['pierre', 'feuille', 'ciseaux']
                        const botChoice = choices[Math.floor(Math.random() * choices.length)]

                        const componentsArray = [
                            {
                                type: 1,
                                components: [ 
                                    { type: 2, style: 'SECONDARY', custom_id: 'pierre', label: 'Pierre', },
                                    { type: 2, style: 'SECONDARY', custom_id: 'feuille', label: 'Feuille', },
                                    { type: 2, style: 'SECONDARY', custom_id: 'ciseaux', label: 'Ciseaux', }
                                ],
                            },
                        ];

                        const msgs = await msg.channel.send({
                            content: ':rock:          :leaves:         :scissors:',
                            components: componentsArray
                        })

                        const filter = button => {
                            return button.user.id === msg.author.id
                        }
                        
                        const button = await msgs.awaitMessageComponent({ filter: filter, componentType: 'BUTTON', max: 1 })

                        if (button.customId === 'pierre') { userEmoji = ':rock:' }     ; if (botChoice === 'pierre')  { botEmoji = ':rock:' }
                        if (button.customId === 'feuille') { userEmoji = ':leaves:' }  ; if (botChoice === 'feuille') { botEmoji = ':leaves:' } 
                        if (button.customId === 'ciseaux') { userEmoji = ':scissors:'} ; if (botChoice === 'ciseaux') { botEmoji = ':scissors:'}

                        const resultPFC = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id};`)
                        msgs.delete()
                        
                        if (button.customId === botChoice) {
                            fonctions.addExperience(bobdb, msg.author.id, 1)

                            if (resultPFC[0].Streak > resultPFC[0].Best) { 
                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Best = Streak WHERE IDUtilisateur = ${msg.author.id};`)
                            }
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Draw = Draw + 1, Streak = 0, Goins = Goins + 5 WHERE IDUtilisateur = ${msg.author.id};`)
                            
                            return button.channel.send({
                                embeds: [ 
                                    exampleEmbed .setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true }
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: `• Égalité ! **+ 5**${goins}`, value: '_ _'}
                                    )
                                ]
                            });
                        }
                        else if ((button.customId === 'pierre' && botChoice === 'ciseaux') ||(button.customId === 'feuille' && botChoice === 'pierre') || (button.customId === 'ciseaux' && botChoice === 'feuille')) {
                            fonctions.addExperience(bobdb, msg.author.id, 1)

                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Gagnant = Gagnant + 1, Streak = Streak + 1, Goins = Goins + 10 WHERE IDUtilisateur = ${msg.author.id};`)
                            if (resultPFC[0].Streak > resultPFC[0].Best) {
                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Best = Streak WHERE IDUtilisateur = ${msg.author.id};`)
                            }

                            return button.channel.send({
                                embeds: [ 
                                    exampleEmbed.setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true }
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: `• Tu as gagné ! **+ 10**${goins}`, value: '_ _'}
                                    )
                                ]
                            })
                        }
                        else {
                            fonctions.addExperience(bobdb, msg.author.id, 1)

                            if (resultPFC[0].Streak > resultPFC[0].Best) {
                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Best = Streak WHERE IDUtilisateur = ${msg.author.id};`)
                            }
                            if (resultPFC[0].Goins < 5) {
                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Perdant = Perdant + 1, Streak = 0, Goins = 0 WHERE IDUtilisateur = ${msg.author.id};`)
                            }
                            else {
                                fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Perdant = Perdant + 1, Streak = 0, Goins = Goins - 5 WHERE IDUtilisateur = ${msg.author.id};`)
                            }
                            
                            
                            return button.channel.send({
                                embeds: [ 
                                    exampleEmbed.setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true }
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: `• Tu as perdu ! **- 5**${goins}`, value: '_ _'}
                                    )
                                ]
                            })
                        }
                    break

                    case 'give':
                        let UserIDGive
                        const DonationAmount = parseInt(args[1])
                        if (args[1] === undefined || isNaN(DonationAmount) || DonationAmount <= 0) {
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                    .setDescription(`Veuillez ajouter un montant de ${goins} à donner`)
                                ]
                            })
                        }
                        else if (args[2] === undefined){
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                    .setDescription(`Veuillez ajouter un utilisateur qui reçoit la donation`)
                                ]
                            })
                        }
                        else {
                            const montant = args[1]
                            UserIDGive = args[2].replace(/[<@!>]/g, "")
                            targetUser = client.users.cache.get(UserIDGive)

                            if (msg.author.id === UserIDGive) {
                                msg.channel.send('Faire un don à vous même ne sert à rien !')
                            }
                            else {
                                const resultGive = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id};`)
                                if (montant > resultGive[0].Goins) { msg.channel.send(`Impossible vous avez seulement ${resultGive[0].Goins} ${goins}`)}
                                else {
                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins + ${montant} WHERE IDUtilisateur = ${UserIDGive};`)
                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins - ${montant} WHERE IDUtilisateur = ${msg.author.id};`)
                                    msg.react('✅')

                                    fonctions.addExperience(bobdb, msg.author.id, 5)
                                }
                            }
                        }
                    break
                    /****************************************************************************************/

                    /**************************** MrBob MAGASIN ******************************/
                    case 'buy':
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            const shopEmoji = ['<:leftarrow:1083130275447451728>', '<:rightarrow:1083129908391317524>']    
                            const resultShop = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Couleurs ORDER BY IDCouleur`)
                            let currentShopPage = 1
                            
                            let i = 0
                            let ColorsFirstPage = ''
                            let ColorsSecondPage = ''
                            for (const row of resultShop) {
                                if (i > 0 && i < 9) {
                                    ColorsFirstPage += `${resultShop[i].Emoji} **${resultShop[i].Nom}** pour **${resultShop[i].Prix}** ${goins}\n\n`
                                }
                                else if (i >= 9) {
                                    ColorsSecondPage += `${resultShop[i].Emoji} **${resultShop[i].Nom}** pour **${resultShop[i].Prix}** ${goins}\n\n`
                                }
                                i++
                            }
                            
                            const sentMessage = await msg.channel.send({
                                embeds: [
                                    exampleEmbed.setTitle(`Le magasin de MrBob`)
                                    .setDescription(ColorsFirstPage)
                                    .setFooter({ text: `Commande d'achat !buy Black - Page ${currentShopPage}/2`, iconURL: '' }) 
                                ]
                            })
                            for (const emoji of shopEmoji) {
                                await sentMessage.react(emoji)
                            }

                            const filters = (reaction, user) => shopEmoji.includes(reaction.emoji.name) && !user.bot
                            const collector = sentMessage.createReactionCollector({ filters, time: 60000 })

                            collector.on('collect', (reaction, user) => {
                                reaction.users.remove(user)
                            
                                if (reaction.emoji.name === 'leftArrow' && !user.bot) {
                                    currentShopPage--
                                    if (currentShopPage < 1) { 
                                        currentShopPage = 2 
                                        MsgEdit = ColorsSecondPage
                                    }
                                    else {
                                        MsgEdit = ColorsFirstPage
                                    }
                                } 
                                else if (reaction.emoji.name === 'rightarrow' && !user.bot) {
                                    currentShopPage++
                                    if (currentShopPage > 2) { 
                                        currentShopPage = 1 
                                        MsgEdit = ColorsFirstPage
                                    }
                                    else {
                                        MsgEdit = ColorsSecondPage
                                    }
                                }
                            
                                sentMessage.edit({ 
                                    embeds: [ 
                                        exampleEmbed.setDescription(MsgEdit) 
                                        .setFooter({ text: `Commande d'achat !buy Black - Page ${currentShopPage}/2`, iconURL: '' })
                                    ]
                                })
                            })
                            
                            collector.on('end', () => { sentMessage.reactions.removeAll().catch(console.error) })
                        }
                        else {
                            let color = msg.content.slice(5)
                            const resultBuy = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Couleurs WHERE Nom = '${color}'`)

                            if (resultBuy !== false) {
                                fonctions.addExperience(bobdb, msg.author.id, 15)

                                const resultAchat = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id};`)
                                if (resultAchat[0].Goins < resultBuy[0].Prix) { 
                                    msg.channel.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`Le magasin de MrBob`)
                                            .setDescription(`Vous n'avez pas assez de Goins${goins}`)
                                        ]
                                    })
                                }
                                else if (resultBuy[0].IDCouleur === resultAchat[0].IDCouleur) {
                                    msg.channel.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`Le magasin de MrBob`)
                                            .setDescription(`Vous possédez déjà cette couleur`)
                                        ]
                                    })
                                }
                                else {
                                    fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET IDCouleur = '${resultBuy[0].IDCouleur}', Goins = Goins - ${resultBuy[0].Prix} WHERE IDUtilisateur = ${msg.author.id};`)
                                    msg.channel.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`Le magasin de MrBob`)
                                            .setDescription(`Votre couleur a bien été ajoutée <:win:1079117243759218699> 
                                            
                                            Elle sera active à la prochaine commande`)
                                        ]
                                    })
                                }
                            }
                            else {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle(`Le magasin de MrBob`)
                                        .setDescription(`La couleur que vous avez choisie n'est pas dans la liste`)
                                    ]
                                })
                            }
                        }                        
                    break

                    case 'b':
                    case 'binouze':
                        const resultBinouze = await fonctions.executeResultQuery(bobdb, `SELECT * FROM InfoUtilisateur WHERE IDUtilisateur = ${msg.author.id};`)
                        if (resultBinouze[0].Goins < 5) { 
                            msg.channel.send(`Vous n'avez pas assez de ${goins} !`)
                        }
                        else {
                            fonctions.addExperience(bobdb, msg.author.id, 2)

                            msg.react('🍺')
                            fonctions.executeSimpleQuery(bobdb, `UPDATE InfoUtilisateur SET Goins = Goins - ${price}, Binouze = Binouze + 1 WHERE IDUtilisateur = ${msg.author.id};`)
                        }
                    break
                    /****************************************************************************************/

                    /**************************** MrBob UTILITAIRE ******************************/
                    case 'gartic':
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            fonctions.addExperience(bobdb, msg.author.id, 2)

                            const resultGartic = await fonctions.executeResultQuery(bobdb, `SELECT IDGartic, Replace(Nom, '_', ' ') as RNom, Lien FROM Gartic_Link ORDER BY IDGartic`)
                            let Titles = ''

                            for (let i = 0; i < resultGartic.length; i++) {                               
                                Titles += '[' + resultGartic[i].RNom + '](' + resultGartic[i].Lien + ')\n'
                            }                                    

                            msg.channel.send({
                                embeds: [ 
                                    exampleEmbed.setTitle('Liste des GIF possible')
                                    .setDescription(`${Titles}`) //\n**Ex: !gartic Lion pressé au zoo**
                                    .setFooter({ text: `Lien cliquable ou !gartic [NOM]`, iconURL: '' })
                                ]
                            });
                        }
                        else {
                            fonctions.addExperience(bobdb, msg.author.id, 2)

                            const argsText = msg.content.toLowerCase().substring(8).replace(/ /g, '_')
                            const resultGartic = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Gartic_Link WHERE Nom = '${argsText}'`)

                            if (resultGartic.length > 0) {
                                msg.channel.send({
                                    embeds: [ 
                                        exampleEmbed.setAuthor({ name: `${msg.author.username} : ${resultGartic[0].Nom.toUpperCase().replace(/_/g, ' ')}`, iconURL: `${msg.author.avatarURL()}`, url: '' })
                                        .setImage(`${resultGartic[0].Lien}`)
                                    ]
                                });
                            }
                            else {
                                msg.channel.send({
                                    embeds: [ 
                                        exampleEmbed.setTitle(`Donnée introuvable`)
                                        .setDescription(`Désolé, je n'ai rien trouvé avec : ${argsText}`)
                                    ]
                                })
                            }
                        }
                    break

                    case 'sd':
                    case 'sondage':
                        if (args[1].endsWith(';')) { args[1] = args[1].slice(0, -1) }
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setTitle('MrBob Syntaxe')
                                    .setDescription('Syntaxe : **$sondage <question>;<réponse 1>;<réponse 2>...**\nLa séparation des réponses ce fait avec "**;**"\n\n:warning: **__Attention__**\n<:rightarrow:1083129908391317524> 9 réponses maximales possibles')
                                ]
                            });
                        }
                        else {
                            const count = msg.content.split(';').length - 1
                            if (count >= 10) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle('MrBob erreur')
                                        .setDescription('Désolé, je suis limité à 9 réponses...')
                                    ]
                                }); 
                            }
                            else {
                                fonctions.addExperience(bobdb, msg.author.id, 4)

                                const args = msg.content.slice(9).split('\n')[0].split(';')
                                const question = args[0]
                                const options = args.slice(1)
                                const emojis = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣']

                                const fields = options.map((option, index) => ({
                                    name: `${emojis[index]} ${option}`, 
                                    value: '_ _'
                                }))

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: 'Sondage lancé par ' + msg.author.username, iconURL: msg.author.displayAvatarURL(), url: ''})
                                        .setTitle(':question: **' + fonctions.capitalizeFirstLetter(question) + '**')
                                        .addFields(fields)
                                    ]
                                })
                                .then(sentMessage => {
                                    for (let i = 0; i < options.length; i++) {
                                        if (i < emojis.length) {
                                            sentMessage.react(emojis[i])
                                        }
                                    }
                                })                        
                            }
                        }
                    break

                    case 'fb':
                    case 'idee':
                    case 'feedback':
                        fonctions.addExperience(bobdb, msg.author.id, 10)

                        let argValue = args[0]
                        let FieldName = ''
                        let FieldValue = ''
                        let Title = ''
                        let Taille = argValue.toString().length + 1

                        Idea = msg.content.substring(Taille)
                        Nom = msg.author.username

                        msg.delete()

                        if (argValue === 'idee') {
                            Title = 'Idée'
                            FieldName = 'Proposition envoyé !'
                            FieldValue = 'Merci pour ton aide ! :heart:'
                        }
                        else {
                            Title = 'Feedback'
                            FieldName = 'Feedback envoyé !'
                            FieldValue = 'Merci pour ton aide ! Ton message va être analysé'
                        }

                        msg.channel.send({
                            embeds: [ 
                                exampleEmbed.setTitle('La boîte à idée !')
                                .addFields(
                                    { name: `${FieldName}`, value: `${FieldValue}`, inline: true }
                                )
                                .setDescription('') 
                            ]
                        })

                        client.users.cache.get('497087235460694038').send({
                            embeds: [
                                exampleEmbed.setTitle(`${Title} de ${Nom}`)
                                .setFields(
                                    { name: 'Information : ', value: Idea, inline: true }
                                )
                                .setDescription('') 
                            ]
                        })
                    break
                    /****************************************************************************************/
                    
                    /**************************** MrBob CLASSEMENT ******************************/
                    case 'top':
                        const resultTop = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, U.Username, Gagnant, Perdant, (Gagnant - Perdant) AS Resultat 
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Utilisateurs U ON IU.IDUtilisateur = U.IDUtilisateur
                            ORDER BY Resultat DESC
                            LIMIT 3;`
                        )
                    
                        fonctions.addExperience(bobdb, msg.author.id, 3)
                    
                        let top3 = ""
                        for (let i = 0; i < 3; i++) {
                            const row = resultTop[i]
                    
                            if (row) {
                                let emojiPlace = ':first_place:'
                                if (i === 1) { emojiPlace = ':second_place:' } else if (i === 2) { emojiPlace = ':third_place:' }
                    
                                top3 += `${emojiPlace}Score : **${resultTop[i].Resultat}** par : **${resultTop[i].Username}**\nAvec **${resultTop[i].Gagnant}** Victoire(s) et **${resultTop[i].Perdant}** Perdue(s)\n\n`
                                
                                if (i === 2) {
                                    msg.channel.send({
                                        embeds: [
                                            exampleEmbed.setTitle('TOP 3 du PFC')
                                            .setDescription(`${top3}`)
                                        ]
                                    })
                                }
                            }
                        }
                    break
                        
                    case 'streak':
                        const resultStreak = await fonctions.executeResultQuery(bobdb, `SELECT IU.IDUtilisateur, U.Username, Best 
                            FROM InfoUtilisateur IU 
                            LEFT JOIN Utilisateurs U ON IU.IDUtilisateur = U.IDUtilisateur
                            ORDER BY Best DESC
                            LIMIT 3;`
                        )
                    
                        fonctions.addExperience(bobdb, msg.author.id, 3)
                    
                        let allStreaks = ""
                        for (let i = 0; i < 3; i++) {
                            let emojiPlace = ':first_place:'
                            if (resultStreak[i].Best > 0) {
                                if (i === 1) { emojiPlace = ':second_place:'} else if (i === 2) { emojiPlace = ':third_place:' }
                                
                                allStreaks += `${emojiPlace} Win Streak : **${resultStreak[i].Best}** par : **${resultStreak[i].Username}**\n`
                            }
                    
                            if (i === 2) {
                                return msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle('Classement Win Streak')
                                        .setDescription(`${allStreaks}`)
                                    ]
                                })
                            }
                        }
                    break
                    /****************************************************************************************/

                    /**************************** MrBob QUOTE ******************************/
                    case 'quote':
                        let fetchMessage
                        if (args[1] === '' || args[1] === null || args[1] === undefined) {
                            const lastMSG = await msg.channel.messages.fetch({ limit: 2 })
                            fetchMessage = lastMSG.last()
                        }
                        else {
                            const messageId = args[1]
                    
                            try {
                                fetchMessage = await msg.channel.messages.fetch(messageId)
                            } 
                            catch (err) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'ID du message n'a pas été trouvé`)
                                    ]
                                })
                                fonctions.logErreur(`${err}`)
                                return
                            }
                        }
                    
                        if (fetchMessage.author.id !== '1078702668685516800') {
                            fonctions.addExperience(bobdb, msg.author.id, 3)
                    
                            const contentMessage = fetchMessage.content
                            const userInfo = fetchMessage.author.id
                            const attachment = fetchMessage.attachments.first()
                            const contentURL = attachment.url

                            if ((contentMessage !== '') && (contentURL === '')) { 
                                fonctions.executeSimpleQuery(bobdb, `INSERT INTO Quotelist (IDUtilisateur, Texte) VALUES (${userInfo}, '${contentMessage}');`) 
                            }
                            else if ((contentMessage === '') && (contentURL !== '')) { 
                                fonctions.executeSimpleQuery(bobdb, `INSERT INTO Quotelist (IDUtilisateur, Texte) VALUES (${userInfo}, '${contentURL}')`) 
                            }
                            else {
                                fonctions.executeSimpleQuery(bobdb, `INSERT INTO Quotelist (IDUtilisateur, Texte) VALUES (${userInfo}, '${contentMessage}\n${contentURL}')`) 
                            }
                            msg.channel.send('Quote ajoutée !')
                        }
                        else {
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`:x: MrBob ne peut pas être quote`)
                                ]
                            })
                        }
                    break
                    
                    case 'ql':
                    case 'quotelist':
                        let UserIDQuotelist = 0
                        if (args[1] === undefined || args[1] === null || args[1] === '') { UserIDQuotelist = msg.author.id } else { UserIDQuotelist = args[1].replace(/[<@!>]/g, "") }
                        targetUser = client.users.cache.get(UserIDQuotelist)
                    
                        const resultQuotelist = await fonctions.executeResultQuery(bobdb, `SELECT * FROM Quotelist WHERE IDUtilisateur = ${UserIDQuotelist} ORDER BY Ordre;`)
                        if (resultQuotelist.length > 0) {
                            fonctions.addExperience(bobdb, msg.author.id, 1)
                    
                            let msgContent = ''
                            let i = 0
                            for (const row of resultQuotelist) {
                                msgContent += '- ' + resultQuotelist[i].Texte + '\n'
                                i++
                            }
                    
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setAuthor({ name: `Quotelist de ` +  targetUser.username , iconURL: targetUser.displayAvatarURL(), url: ''})
                                    .setDescription(msgContent)
                                ]
                            })
                        }
                        else {
                            msg.channel.send(`${targetUser.username} n'a pas de quotelist pour le moment`)
                        }
                    break
                    /****************************************************************************************/

                    /**************************** MrBob USELESS ******************************/
                    case 'rage':
                        fonctions.addExperience(bobdb, msg.author.id, 4)
                        msg.delete()
                    
                        let fetchedMessage
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            const messages = await msg.channel.messages.fetch({ limit: 2 })
                            fetchedMessage = messages.last()
                        } 
                        else {
                            try {
                                fetchedMessage = await msg.channel.messages.fetch(args[1])
                            } 
                            catch (err) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'ID du message n'a pas été trouvé`)
                                    ]
                                })
                                fonctions.logErreur(`${err}`)
                            }
                        }
                    
                        const msgContent = fetchedMessage.content
                        let alphabetCount = 0
                        let result = ""
                        for (let i = 0; i < msgContent.length; i++) {
                            const character = msgContent[i]
                    
                            if (character !== ' ' && character.match(/[a-zA-Z]/)) {
                                if (alphabetCount % 2 === 0) { result += character.toUpperCase() }
                                else { result += character.toLowerCase() }
                                alphabetCount++
                            } 
                            else { result += character }
                        }
                    
                        msg.channel.send(result)
                    break
                    /****************************************************************************************/

                    /**************************** MrBob INFORMATIONS ******************************/
                    case 'up':
                    case 'update':
                        const paginationEmojis = ['<:leftarrow:1083130275447451728>', '<:rightarrow:1083129908391317524>']
                        const pages = await fonctions.getFichiers('versionning') 
                        let currentPage = pages.length - 1
                    
                        const sentMessage = await msg.channel.send({
                            embeds: [ 
                                exampleEmbed.setTitle('MrBob Updates')
                                    .setDescription(fs.readFileSync(`./versionning/${pages[currentPage]}`, 'utf8')) 
                                    .setFooter({ text: `Page ${currentPage + 1}/${pages.length}`, iconURL: client.user.displayAvatarURL() })
                            ]
                        })
                        for (const emoji of paginationEmojis) {
                            await sentMessage.react(emoji)
                        }
                    
                        const filters = (reaction, user) => paginationEmojis.includes(reaction.emoji.name) && !user.bot
                        const collector = sentMessage.createReactionCollector({ filters, time: 60000 })

                        collector.on('collect', (reaction, user) => {
                            reaction.users.remove(user)
                    
                            if (reaction.emoji.name === 'leftArrow') {
                                currentPage--
                                if (currentPage < 0) { currentPage = pages.length - 1 }
                            } 
                            else if (reaction.emoji.name === 'rightarrow') {
                                currentPage++
                                if (currentPage >= pages.length) { currentPage = 0 }
                            }
                    
                            sentMessage.edit({ 
                                embeds: [ 
                                    exampleEmbed.setDescription(fs.readFileSync(`./versionning/${pages[currentPage]}`, 'utf8')) 
                                        .setFooter({ text: `Page ${currentPage + 1}/${pages.length}`, iconURL: client.user.displayAvatarURL() })
                                ]
                            })
                        })
                    
                        collector.on('end', () => { sentMessage.reactions.removeAll().catch(console.error) })
                    break

                    case 'v':
                    case 'version':
                        fonctions.addExperience(msg.author.id, 1)
                        msg.delete()
                    
                        msg.channel.send({
                            embeds: [ 
                                exampleEmbed.setTitle('MrBob Version')
                                .setDescription('Je suis actuellement en ' + fs.readFileSync('./version.txt', 'utf8')) 
                            ]
                        })
                    break
                        
                    case 'info':
                        fonctions.addExperience(msg.author.id, 1)
                        msg.delete()
                    
                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setTitle('MrBob Informations')
                                .setDescription(':robot: Je suis un bot développé par Wookis#6378.\n:bulb: Si vous voulez me donner des idées faites : ```!idee [ MESSAGE ]```\n <:twitter:1079088903895650364> https://twitter.com/Wookkis')
                            ]
                        })
                    break
                    /****************************************************************************************/
                }
            }
            catch(err){
                fonctions.logErreur(`${err.message}`)
            }
        }
    }
});

client.login(auth.token);