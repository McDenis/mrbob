:gear: **__MrBob Utilitaire__**
`!gartic [ TITRE ]` • Affiche les titres afin de voir le GIF d'un Gartic Phone 
`!sondage ou !sd [ QUESTION;REPONSES ]` • Permet de créer un sondage 
`!idee [ MESSAGE ]` • Permet de proposé des idées d'amélioration
`!feedback ou !fb [ MESSAGE ]` • Permet de faire un retour sur un crash/bug 

:medal: **__MrBob Classements__**
`!top` • Permet d'afficher le Top 3 des joueurs du Pierre, Feuille, Ciseaux
`!streak` • Permet d'afficher la personne et le score de la meilleure Win Streak

:speech_balloon: **__MrBob Quote__**
`!quote [ ID ]` • Quote le message précédent ou celui passé en paramètre
`!quotelist ou !ql [ @user ]` • Afficher sa propre quotelist ou celui d'un utilisateur 

:no_entry_sign: **__MrBob Useless__**
`!rage [ ID ]` • Rage sur le message précédent ou l'ID donné