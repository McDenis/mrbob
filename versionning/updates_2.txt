• **0.1.4** :
- Ajout des pages sur la commande !update
- Ajout de !aide gartic
    
• **0.1.5** :
- Ajout de la commande !rage
- Mise à jour du README.md

• **0.1.5.1** :
- Correction et modification de !rage
- Mise à jour de la commande !aide
- Mise à jour du README.md

• **0.1.6** :
- Ajout de la commannde !xp et !sondage
- Mise à jour de l'aide, du README de la version

• **0.1.6.2** :
- Test de !play pour jouer des sons dans un vocal
- Ajout de la date/heure devant le message d'erreur
- Mise à jour discord.js vers la v13.15.1
- Mise à jour du !aide, !pfc et README.md 