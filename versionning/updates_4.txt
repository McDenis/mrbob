• **0.2.3** :
- Mise à jour interface de certaines commandes
- Ajout de la commande !badge et !work
- Mise à jour des fichiers nécessaire 

• **0.2.4** :
- Ajout de la commande !rank (anciennement !gb)
- Modification de la commande !gamble (!gb)
- Mise à jour de la commande !gartic
- Mise à jour des fichiers nécessaire 

• **0.2.5** :
- Suppression de la commande !badge
- Ajout de la commande !job
- Trie des commandes dans le main.js (correspond à l'aide)
- Mise à jour de la commande !rank 
- Mise à jour des fichiers nécessaire 

• **0.2.6** :
- Ajout de !jobs et !ranks : affiche toute la liste
- Mise à jour de !buy : aperçu de la couleur
- Mise à jour visuel de certaines commandes