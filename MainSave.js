const { Client, Intents, MessageEmbed } = require('discord.js')

const auth = require('./auth.json');
const mysql = require('mysql')
const fs = require('fs');

const client = new Client({ 
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_VOICE_STATES]
});

client.fonctions = new Discord.Collection();

const functionsFile = fs.readdirSync('./functions/').filter(file => file.endsWith('.js'));
for(const file of functionsFile){
    const fonction = require(`./functions/${file}`);
    client.fonctions.set(fonction.name, fonction);
}

const bobdb = mysql.createConnection({
    host: 'localhost',
    user: 'bob',
    password: 'mrbob',
    database: 'mrbob'
})

client.on('ready', function (evt) {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({ 
        activities: [{
            name: "!aide",
            type: 'LISTENING',
        }],
        status: 'online'
    });
});

bobdb.connect(function(err) {
    if (err) { logErreur(err) } 
    else { console.log('Connected to MrBob database!') }
})

const cats = [ 'spooky', 'snow', 'nono', 'vanille', 'siouxie' ]
const goins = '<:goins:1080160243595628644>'

function verifFichier(userID) {
    if (!fs.existsSync('./users/' + userID + '.json')) {
        var obj = { pfc: [] }
        obj.pfc.push({ gagnant: 0 }, { perdant: 0 }, { draw: 0 }, { streak: 0 }, { best: 0 }, { level: 1 }, { xp: 0 }, { nextlvl: 50 })
        var json = JSON.stringify(obj);
        fs.writeFileSync('./users/' + userID + '.json', json, 'utf-8');
    }
}

function verifQuotelist(userID) {
    if (!fs.existsSync('./quotelist/' + userID + '.txt')) {
        fs.appendFileSync('./quotelist/' + userID + '.txt', '');
    }
}

function logErreur(error) {
    var d = new Date()
    fs.appendFileSync('./erreurs/erreurs.log', `[${d.toLocaleString()}] - ${error}\n`)
}

client.on('messageCreate', async msg => {
    if (msg.author.bot) return;

    if (msg.author.id !== client.user.id && msg.author.id !== '432610292342587392'){
        const exampleEmbed = new MessageEmbed()
        .setColor(0x407676) 
        .setThumbnail(client.user.displayAvatarURL)
        // .setTimestamp()
        // .setFooter({ text: msg.author.username, iconURL: msg.author.displayAvatarURL });

        message = msg.toString()

        // Pour répondre 'FEUR' 
        if (message.toLowerCase().match(/quoi(\s*\?)?$/)) {
            choice = Math.floor(Math.random() * 2)
            switch(choice) {
                case 0:
                    msg.channel.send('feur !')
                break
                case 1:
                    msg.channel.send('drilatère !')
                break
            }            
        }
        
        // Envoie l'image de l'astronobinouze
        if (msg.content.toLowerCase().includes('astronobinouze')) {
            msg.channel.send({ files: [{ attachment: './images/IMG_MrBob/MrBob_Binouze_NoBackground.png' }] })
        }

        // GIF 'ENVIE DE CREVER' de Vilebrequin
        if (msg.content.toLowerCase().includes('envie de crever') || msg.content.toLowerCase().includes('envie de crevé')) {
            msg.channel.send(
                'https://tenor.com/view/vilebrequin-vilbrequin-sylvain-pierre-sylvain-vilebrequin-gif-23624159'
            )
        }

        // React 'chicken' à HFC
        if (msg.content.toLowerCase().includes('hfc')) {
            msg.react('🐔')
        }

        // Chaque chat à sa couronne
        if (cats.some(cat => msg.content.toLowerCase().includes(cat))) {
            msg.react('👑');
        }

        //Rompish time
        if (msg.content.toLowerCase().includes('rompish')) {
            msg.react('<:rompish:1080186651348840599>')
        }

        // Toutes les commandes
        if (message.substring(0,1) == "!"){
            /**************************** START FUNCTIONS ******************************/
            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            function read_userID(userID) {
                Read = fs.readFileSync('./users/' + userID + '.json', 'utf8');
                Read = JSON.parse(Read);
            }
            function write_userID(userID) {
                json = JSON.stringify(Read);
                fs.writeFileSync('./users/' + userID + '.json', json);
            }
            function writeQuotelist(userID, content) {
                fs.appendFileSync('./quotelist/' + userID + '.txt', `- ${content}\n`)
            }
            
            function addExperience(userID, xpToAdd) {
                verifFichier(userID)

                Read = fs.readFileSync('./users/' + userID + '.json', 'utf8');
                Read = JSON.parse(Read);
                
                if (Read.pfc[6].xp + xpToAdd >= Read.pfc[7].nextlvl) {
                    Read.pfc[5].level += 1
                    Read.pfc[6].xp = 0
                    Read.pfc[7].nextlvl = Read.pfc[7].nextlvl * 2
                } 
                else {
                    Read.pfc[6].xp += xpToAdd
                }

                json = JSON.stringify(Read);
                fs.writeFileSync('./users/' + userID + '.json', json);
            }

            /**************************** END FUNCTIONS ******************************/

            /**************************** START COMMANDS ******************************/
            let args = message.toLowerCase().substring(1).split(' ');
            let cmd = args[0];

            try{
                switch(cmd) {
                    case 'test':
                        checkUser(bobdb, `SELECT * FROM utilisateurs WHERE IDUtilisateur = ${msg.author.id};`)
                        .then(result => {
                            if (result === false) {
                                console.log('Faux')
                            }
                            else { console.log('Vrai') }
                        })
                        .catch(error => {
                            logErreur(error)
                        })
                    break
                    // Récupére l'ID de l'emoji mis en param
                    case 'getid':
                        if (msg.author.id === '497087235460694038') {
                            msg.author.send(args[1].replace(/[<>]/g, ""))
                        }
                    break;

                    // Aide des commandes (envoyé en privé)
                    case 'aide':
                        addExperience(msg.author.id, 2)
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            Read = fs.readFileSync('./messages/aide.txt', 'utf8')
                            msg.author.send({
                                embeds: [
                                    exampleEmbed.setTitle(`MrBob est là pour t'aider`)
                                    .addFields(
                                        { name: 'Commandes:', value: `${Read}` },
                                    )
                                    .setDescription('') 
                                ]
                            });
                        }
                        else {
                            switch(args[1]){
                                case 'pfc':
                                    Read = fs.readFileSync('./messages/aide_pfc.txt', 'utf8')
                                    msg.author.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`MrBob est là pour t'aider`)
                                            .addFields(
                                                { name: '__Les Règles du Pierre, Feuille, Ciseaux__ :', value: `${Read}` },
                                            )
                                            .setDescription('') 
                                        ]
                                    });
                                break;
                                case 'gartic':
                                    Read = fs.readFileSync('./messages/aide_gartic.txt', 'utf8')
                                    msg.author.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`MrBob est là pour t'aider`)
                                            .addFields(
                                                { name: '__Les Règles de Gartic__ :', value: `${Read}` },
                                            )
                                            .setDescription('') 
                                        ]
                                    });
                                break;
                                case 'sondage':
                                    Read = fs.readFileSync('./messages/aide_sondage.txt', 'utf8')
                                    msg.author.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`MrBob est là pour t'aider`)
                                            .addFields(
                                                { name: '__Comment fonctionne le sondage ?__ :', value: `${Read}` },
                                            )
                                            .setDescription('') 
                                        ]
                                    });
                                break;
                                default:
                                    Read = fs.readFileSync('./messages/aide.txt', 'utf8')
                                    msg.author.send({
                                        embeds: [
                                            exampleEmbed.setTitle(`MrBob est là pour t'aider`)
                                            .addFields(
                                                { name: 'Commandes:', value: `${Read}` },
                                            )
                                            .setDescription('') 
                                        ]
                                    });
                                break;
                            }
                        }
                    break;

                    // Afficher son profil d'XP ou celui d'un utilisateur
                    case 'xp':
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            verifFichier(msg.author.id)
                            read_userID(msg.author.id)
                            
                            userLevel = Read.pfc[5].level
                            userXP = Read.pfc[6].xp
                            XPMax = Read.pfc[7].nextlvl
                            XPNextLevel = XPMax - userXP
                            userProgression = Math.round((userXP * 100 / XPMax), 2)

                            emojiProgression = '<:5pc:1106881992273973370>'
                            if (userProgression >= 25 && userProgression < 50) { 
                                emojiProgression = '<:25pc:1106881994341761065>'
                            } 
                            else if (userProgression >= 50 && userProgression < 90) {
                                emojiProgression = '<:50pc:1106881995566481568>'
                            }
                            else if (userProgression >= 90) {
                                emojiProgression = '<:90pc:1106881996883509320>'
                            }

                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setTitle(`Profil d'expérience`)
                                    .addFields(
                                        { name: `Level Actuel`, value: `<:level:1106875098075508756> ${userLevel}` },  
                                        { name: '_ _', value: '_ _'}
                                    )
                                    .addFields(
                                        { name: 'Expérience', value: `<:exp:1106876766422835261> ${userXP}`, inline: true },
                                        { name: 'XP Nécessaire', value: `<:xpup:1106879182228701187> ${XPNextLevel}`, inline: true },    
                                        { name: 'Progression', value: `${emojiProgression} ${userProgression} %`, inline: true }                                          
                                    )
                                    .setAuthor({ name: `Profil de ${msg.author.username}`, iconURL: `${msg.author.avatarURL()}`, url: '' })
                                ]
                            });
                        }
                        else {
                            if (fs.existsSync('./users/' + args[1].replace(/[<@!>]/g, "") + '.json')) {
                                addExperience(msg.author.id, 2)
                                LeUserID = args[1].replace(/[<@!>]/g, "")
                                targetUser = client.users.cache.get(LeUserID);

                                verifFichier(LeUserID)
                                read_userID(LeUserID)

                                userLevel = Read.pfc[5].level
                                userXP = Read.pfc[6].xp
                                XPMax = Read.pfc[7].nextlvl
                                XPNextLevel = XPMax - userXP
                                userProgression = Math.round((userXP * 100 / XPMax), 2)

                                emojiProgression = '<:5pc:1106881992273973370>'
                                if (userProgression >= 25 && userProgression < 50) { 
                                    emojiProgression = '<:25pc:1106881994341761065>'
                                } 
                                else if (userProgression >= 50 && userProgression < 90) {
                                    emojiProgression = '<:50pc:1106881995566481568>'
                                }
                                else if (userProgression >= 90) {
                                    emojiProgression = '<:90pc:1106881996883509320>'
                                }

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle(`Profil d'expérience`)
                                        .addFields(
                                            { name: `Level Actuel`, value: `<:level:1106875098075508756> ${userLevel}` },  
                                            { name: '_ _', value: '_ _'}
                                        )
                                        .addFields(
                                            { name: 'Expérience', value: `<:exp:1106876766422835261> ${userXP}`, inline: true },
                                            { name: 'XP Nécessaire', value: `<:xpup:1106879182228701187> ${XPNextLevel}`, inline: true },    
                                            { name: 'Progression', value: `${emojiProgression} ${userProgression} %`, inline: true }                                          
                                        )
                                        .setAuthor({ name: `Profil de ${msg.author.username}`, iconURL: `${msg.author.avatarURL()}`, url: '' })
                                    ]
                                });
                            }
                            else {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle(`Joueur introuvable !`)
                                        .setDescription('Le joueur n\'existe pas, réessaie avec un autre.') 
                                    ]
                                });
                            }
                        }
                    break;
                    
                    // Afficher son profil du Pierre, Feuille, Ciseaux ou celui d'un utilisateur
                    case 'profil':
                        addExperience(msg.author.id, 2)
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            verifFichier(msg.author.id)
                            read_userID(msg.author.id)
                            
                            total  = Read.pfc[0].gagnant + Read.pfc[1].perdant + Read.pfc[2].draw
                            winner = Read.pfc[0].gagnant
                            loser  = Read.pfc[1].perdant
                            egalit = Read.pfc[2].draw
                            winrate = ((2 * winner) + egalit) / (2 * total) * 100;

                            if (total > 1) { pText = 'parties' } else { pText = 'partie'}

                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setTitle(`Résultat sur ${total} ${pText}`)
                                    .addFields(
                                        { name: `Meilleure Win Streak`, value: `:medal: ${Read.pfc[4].best}` },  
                                        { name: '_ _', value: '_ _'}
                                    )
                                    .addFields(
                                        { name: 'Gagnée(s)', value: `<:win:1079117243759218699> ${winner}`, inline: true },
                                        { name: 'Perdue(s)', value: `:x: ${loser}`, inline: true },
                                        { name: 'Égalité(s)', value: `<:draw:1079118690743750697> ${egalit}`, inline: true }                                              
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: 'Pourcentage de victoire', value: `:trophy: ${winrate.toFixed(2)}%`}
                                    )
                                    .setAuthor({ name: `Profil de ${msg.author.username}`, iconURL: `${msg.author.avatarURL()}`, url: '' })
                                ]
                            });
                        }
                        else {
                            if (fs.existsSync('./users/' + args[1].replace(/[<@!>]/g, "") + '.json')) {
                                LeUserID = args[1].replace(/[<@!>]/g, "")
                                targetUser = client.users.cache.get(LeUserID);

                                verifFichier(LeUserID)
                                read_userID(LeUserID)

                                total = Read.pfc[0].gagnant + Read.pfc[1].perdant + Read.pfc[2].draw
                                winner = Read.pfc[0].gagnant
                                loser = Read.pfc[1].perdant
                                egalit = Read.pfc[2].draw
                                winrate = (2 * winner + egalit) / (2 * total) * 100

                                if (total > 1) { pText = 'parties' } else { pText = 'partie'}

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle(`Résultat sur ${total} ${pText}`)
                                        .addFields(
                                            { name: `Meilleure Win Streak`, value: `:medal: ${Read.pfc[4].best}` },  
                                            { name: '_ _', value: '_ _'}
                                        )
                                        .addFields(
                                            { name: 'Gagnée(s)', value: `<:win:1079117243759218699> ${winner}`, inline: true },
                                            { name: 'Perdue(s)', value: `:x: ${loser}`, inline: true },
                                            { name: 'Égalité(s)', value: `<:draw:1079118690743750697> ${egalit}`, inline: true }
                                        )
                                        .addFields(
                                            { name: '_ _', value: '_ _'},
                                            { name: '• Pourcentage de victoire', value: `:trophy: ${winrate.toFixed(2)}%`}
                                        )
                                        .setAuthor({ name: `Profil de ${targetUser.username}`, iconURL: `${targetUser.avatarURL()}`, url: '' })
                                    ]
                                });
                            }
                            else {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle(`Joueur introuvable !`)
                                        .setDescription('Le joueur n\'existe pas, réessaie avec un autre.') 
                                    ]
                                });
                            }
                        }
                    break;

                    // Boite à idée
                    case 'idee':
                        addExperience(msg.author.id, 10)

                        idea = msg.content.substring(5);
                        nom = msg.author.username
                        msg.delete()

                        msg.channel.send({
                            embeds: [ 
                                exampleEmbed.setTitle('La boîte à idée !')
                                .addFields(
                                    { name: 'Proposition envoyé !', value: 'Merci pour ton aide ! :heart:', inline: true },
                                )
                                .setDescription('') 
                            ]
                        });

                        client.users.cache.get('497087235460694038').send({
                            embeds: [
                                exampleEmbed.setTitle(`Idée de ${nom}`)
                                .setFields(
                                    { name: 'Proposition : ', value: idea, inline: true },
                                )
                                .setDescription('') 
                            ]
                        });
                    break;

                    // Jeu du Pierre, Feuille, Ciseaux
                    case 'pfc':
                        verifFichier(msg.author.id)
                        addExperience(msg.author.id, 1)

                        const choices = ['pierre', 'feuille', 'ciseaux']
                        const botChoice = choices[Math.floor(Math.random() * choices.length)];

                        const componentsArray = [
                            {
                                type: 1,
                                components: [ 
                                    { type: 2, style: 'SECONDARY', custom_id: 'pierre', label: 'Pierre', },
                                    { type: 2, style: 'SECONDARY', custom_id: 'feuille', label: 'Feuille', },
                                    { type: 2, style: 'SECONDARY', custom_id: 'ciseaux', label: 'Ciseaux', },
                                ],
                            },
                        ];

                        const msgs = await msg.channel.send({
                            content: ':rock:          :leaves:         :scissors:',
                            components: componentsArray,
                        })

                        const filter = button => {
                            return button.user.id === msg.author.id;
                        }
                        
                        const button = await msgs.awaitMessageComponent({ filter: filter, componentType: 'BUTTON', max: 1 });

                        if (button.customId === 'pierre') { userEmoji = ':rock:' }     ; if (botChoice === 'pierre')  { botEmoji = ':rock:' }
                        if (button.customId === 'feuille') { userEmoji = ':leaves:' }  ; if (botChoice === 'feuille') { botEmoji = ':leaves:' } 
                        if (button.customId === 'ciseaux') { userEmoji = ':scissors:'} ; if (botChoice === 'ciseaux') { botEmoji = ':scissors:'}

                        if (button.customId === botChoice) {
                            read_userID(msg.author.id)
                            if (Read.pfc[3].streak > Read.pfc[4].best) { Read.pfc[4].best = Read.pfc[3].streak}
                            Read.pfc[2].draw += 1
                            Read.pfc[3].streak = 0
                            write_userID(msg.author.id);

                            return button.reply({
                                embeds: [ 
                                    exampleEmbed .setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true },
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: '• Égalité !', value: '_ _'}
                                    )
                                ]
                            });
                        }
                        else if ((button.customId === 'pierre' && botChoice === 'ciseaux') ||(button.customId === 'feuille' && botChoice === 'pierre') || (button.customId === 'ciseaux' && botChoice === 'feuille')) {
                            read_userID(msg.author.id)
                            Read.pfc[0].gagnant += 1
                            Read.pfc[3].streak += 1
                            if (Read.pfc[3].streak > Read.pfc[4].best) { Read.pfc[4].best = Read.pfc[3].streak}
                            write_userID(msg.author.id);

                            return button.reply({
                                embeds: [ 
                                    exampleEmbed.setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true },
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: '• Tu as gagné !', value: '_ _'}
                                    )
                                ]
                            })
                        }
                        else {
                            read_userID(msg.author.id)
                            if (Read.pfc[3].streak > Read.pfc[4].best) { Read.pfc[4].best = Read.pfc[3].streak}
                            Read.pfc[1].perdant += 1
                            Read.pfc[3].streak = 0
                            write_userID(msg.author.id);

                            return button.reply({
                                embeds: [ 
                                    exampleEmbed.setTitle('Pierre, Feuille, Ciseaux')
                                    .addFields(
                                        { name: 'J\'ai joué :', value: `${botChoice.replace(/^\w/, c => c.toUpperCase())} ${botEmoji}` , inline: true },
                                        { name: 'Tu as joué :', value: `${button.customId.replace(/^\w/, c => c.toUpperCase())} ${userEmoji}`, inline: true },
                                    )
                                    .addFields(
                                        { name: '_ _', value: '_ _'},
                                        { name: '• Tu as perdu !', value: '_ _'}
                                    )
                                ]
                            })
                        }
                    break;

                    // Affiche la liste ou le Gartic en param
                    case 'gartic':
                        addExperience(msg.author.id, 2)
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            const filePath = fs.readFileSync('./liens/gartic_liens.txt', 'utf8')
                            let titles = '';

                            const lines = filePath.split('\n');
                            for (let i = 0; i < lines.length; i++) {
                                const [text, link] = lines[i].split(' : ');
                                
                                if (!text.includes('https')) {
                                    titles += '- [' + text + '](' + link + ')\n' //`- ${text}\n`;
                                }
                            }                                    

                            msg.channel.send({
                                embeds: [ 
                                    exampleEmbed.setTitle('Liste des GIF possible')
                                    .setDescription(`${titles}`) //\n**Ex: !gartic Lion pressé au zoo**
                                ]
                            });
                        }
                        else {
                            const filePath = fs.readFileSync('./liens/gartic_liens.txt', 'utf8')
                            const text = msg.content.toLowerCase().substring(8);
                            const lines = filePath.toLowerCase().split('\n');
                            let bFound = 0

                            for (let i = 0; i < lines.length; i++) {
                                const [ txt, link ] = lines[i].split(' : ')
                                if (txt === (text)) {
                                        bFound = 1
                                        msg.channel.send({
                                            embeds: [ 
                                                exampleEmbed//.setTitle(`${text.replace(/^\w/, c => c.toUpperCase())}`)
                                                .setAuthor({ name: `${msg.author.username} : ${text.toUpperCase()}`, iconURL: `${msg.author.avatarURL()}`, url: '' })
                                                .setImage(`${link}`)
                                                .setDescription(``)
                                            ]
                                        });
                                    break; 
                                }
                            }
                            if (bFound !== 1) {
                                msg.channel.send({
                                    embeds: [ 
                                        exampleEmbed.setTitle(`Donnée introuvable`)
                                        .setDescription(`Désolé, je n'ai rien trouvé avec : ${text}`)
                                    ]
                                });
                            }
                        }
                    break;
                    
                    // Affiche le top 3 des meilleurs joueurs (Pierre, Feuille, Ciseaux)
                    case 'top':
                        addExperience(msg.author.id, 3)

                        const folderPath = './users/';
                        let allPlayersData = [];

                        fs.readdirSync(folderPath).forEach(file => {
                            Read = fs.readFileSync('./users/' + file, 'utf8');
                            Read = JSON.parse(Read);

                            allPlayersData.push({ win: Read.pfc[0].gagnant, lose: Read.pfc[1].perdant, score: (Read.pfc[0].gagnant - Read.pfc[1].perdant), id: file.substring(0, file.length - 5)});
                        });
                        allPlayersData.sort((a, b) => b.score - a.score);

                        let top3 = "";
                        for (let i = 0; i < Math.min(allPlayersData.length, 3); i++) {
                            const player = allPlayersData[i];

                            client.users.fetch(player.id)
                            .then(user => {
                                const pseudo = user.username || '[Hors serveur]';

                                let emojiPlace = ':first_place:'
                                if (i === 1) { 
                                emojiPlace = ':second_place:'
                                } else if (i === 2) {
                                emojiPlace = ':third_place:'
                                }

                                top3 += `${emojiPlace}Score : **${player.score}** par : **${pseudo}**\nAvec **${player.win}** Victoire(s) et **${player.lose}** Perdue(s)\n\n`;

                                // Vérifie si c'est la dernière itération de la boucle
                                if (i === Math.min(allPlayersData.length, 3) - 1) {
                                    msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle('TOP 3 du PFC')
                                        .setDescription(`${top3}`)
                                    ]
                                    });
                                }
                            })
                            .catch(error => {
                                logErreur(`Erreur on !top : ${error}`)
                            });
                        }
                    break;

                    // WORK IN PROGRESS 
                    case 'bj':
                        const WIPLink = 'https://i.imgur.com/VP0KBTI.png'
                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setTitle('MrBob WIP')
                                .setImage(`${WIPLink}`)
                                .setDescription('Work in progress...')
                            ]
                        });
                    break; 

                    // Affiche le joueur ayant fait le plus de jeu gagnant d'affilé
                    case 'streak':
                        addExperience(msg.author.id, 3)

                        const foldPath = './users/';
                        let allPlayerData = [];

                        fs.readdirSync(foldPath).forEach(file => {
                            Read = fs.readFileSync('./users/' + file, 'utf8');
                            Read = JSON.parse(Read);

                            allPlayerData.push({ streak: Read.pfc[4].best, id: file.substring(0, file.length - 5) });
                        });
                        allPlayerData.sort((a, b) => b.streak - a.streak);

                        let allStreaks = "";
                        for (let i = 0; i < allPlayerData.length; i++) {
                            const player = allPlayerData[i];
                            client.users.fetch(player.id)
                            .then(user => {
                                const pseudo = user.username
                                let emojiPlace = '<:ssl:1083461336249421874>'

                                if (player.streak > 0) {
                                    if (i === 1) { emojiPlace = '<:GC3:1083073122825142313>'}
                                    else if (i === 2) { emojiPlace = '<:C3:1083073279432073317>' }
                                    
                                    allStreaks += `${emojiPlace} Win Streak : **${player.streak}** par : **${pseudo}**\n`
                                }

                                if (i === allPlayerData.length - 1) {
                                    msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle('Classement Win Streak')
                                        .setDescription(`${allStreaks}`)
                                    ]
                                    });
                                }
                            })
                            .catch(error => {
                                logErreur(`Erreur on !streak : ${error}`)
                            });
                        }
                    break;

                    // Met en "rage" le message précédent ou celui mis en param
                    case 'rage':
                        addExperience(msg.author.id, 4)

                        msg.delete()

                        let fetchedMessage

                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            const messages = await msg.channel.messages.fetch({ limit: 2 })
                            fetchedMessage = messages.last()
                        } 
                        else {
                            try {
                                fetchedMessage = await msg.channel.messages.fetch(args[1])
                            } 
                            catch (err) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'ID du message n'a pas été trouvé`)
                                    ]
                                })
                                logErreur(`${err.message}`)
                                return
                            }
                        }

                        const msgContent = fetchedMessage.content
                        let alphabetCount = 0
                        let result = ""

                        for (let i = 0; i < msgContent.length; i++) {
                            const character = msgContent[i]

                            if (character !== ' ' && character.match(/[a-zA-Z]/)) {
                                if (alphabetCount % 2 === 0) {
                                    result += character.toUpperCase()
                                }
                                else {
                                    result += character.toLowerCase()
                                }
                                alphabetCount++
                            } 
                            else {
                                result += character
                            }
                        }

                        msg.channel.send(result);
                    break;

                    // Permet de créer un sondage (max 9 réponse)
                    case 'sondage':
                        if (args[1].endsWith(';')) { args[1] = args[1].slice(0, -1) }
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setTitle('MrBob Syntaxe')
                                    .setDescription('Syntaxe : **$sondage <question>;<réponse 1>;<réponse 2>...**\nLa séparation des réponses ce fait avec "**;**"\n\n:warning: **__Attention__**\n<:rightarrow:1083129908391317524> 9 réponses maximales possibles')
                                ]
                            });
                        }
                        else {
                            const count = msg.content.split(';').length - 1
                            if (count >= 10) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setTitle('MrBob erreur')
                                        .setDescription('Désolé, je suis limité à 9 réponses...')
                                    ]
                                }); 
                            }
                            else {
                                addExperience(msg.author.id, 4)
                                const args = msg.content.slice(9).split('\n')[0].split(';');
                                const question = args[0];
                                const options = args.slice(1);
                                const emojis = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣'];

                                const fields = options.map((option, index) => ({
                                    name: `${emojis[index]} ${option}`, 
                                    value: '_ _',
                                }));

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: 'Sondage lancé par ' + msg.author.username, iconURL: msg.author.displayAvatarURL(), url: ''})
                                        .setTitle(':question: **' + capitalizeFirstLetter(question) + '**')
                                        .addFields(fields)
                                    ]
                                })
                                .then(sentMessage => {
                                    for (let i = 0; i < options.length; i++) {
                                        if (i < emojis.length) {
                                            sentMessage.react(emojis[i]);
                                        }
                                    }
                                });                            
                            }
                        }
                    break;

                    case 'quote':
                        addExperience(msg.author.id, 3)
                        let fetchMessage

                        if (args[1] === '' || args[1] === null || args[1] === undefined) {
                            const lastMSG = await msg.channel.messages.fetch({ limit: 2 })
                            fetchMessage = lastMSG.last()
                        }
                        else {
                            const messageId = args[1]

                            try {
                                fetchMessage = await msg.channel.messages.fetch(messageId)
                            } 
                            catch (err) {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'ID du message n'a pas été trouvé`)
                                    ]
                                })
                                logErreur(`${err}`)
                                return
                            }
                        }

                        if (fetchMessage.author.id !== '1078702668685516800') {
                            const contentMessage = fetchMessage.content
                            const userInfo = fetchMessage.author.id

                            const attachment = fetchMessage.attachments.first();
                            const contentURL = attachment.url;

                            verifQuotelist(userInfo)
                            if (contentMessage !== '') {
                                writeQuotelist(userInfo, contentMessage)
                            }
                            else {
                                writeQuotelist(userInfo, contentURL)
                            }
                            msg.channel.send('Quote ajoutée !')
                        }
                        else {
                            msg.channel.send({
                                embeds: [
                                    exampleEmbed.setDescription(`MrBob ne peut pas être quote`)
                                ]
                            })
                        }
                    break;

                    case 'quotelist':
                        addExperience(msg.author.id, 1)
                        if (args[1] === '' || args[1] === null || args[1] === undefined) {
                            if (fs.existsSync('./quotelist/' + msg.author.id + '.txt', 'utf8')) {
                                const msgConst = fs.readFileSync('./quotelist/' + msg.author.id + '.txt', 'utf8')

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: `Quotelist de ` +  msg.author.username , iconURL: msg.author.displayAvatarURL(), url: ''})
                                        .setDescription(msgConst)
                                    ]
                                })
                            }
                            else {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'utilisateur n'a pas été trouvé`)
                                    ]
                                })
                            }
                        }
                        else {
                            if (fs.existsSync('./quotelist/' + args[1].replace(/[<@!>]/g, "") + '.txt')) {
                                addExperience(msg.author.id, 2)
                                
                                LeUserID = args[1].replace(/[<@!>]/g, "")
                                targetUser = client.users.cache.get(LeUserID);

                                const msgConst = fs.readFileSync('./quotelist/' + LeUserID + '.txt', 'utf8')

                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: `Quotelist de ` +  targetUser.username , iconURL: targetUser.displayAvatarURL(), url: ''})
                                        .setDescription(msgConst)
                                    ]
                                })

                                verifFichier(LeUserID)
                                read_userID(LeUserID)
                            }
                            else {
                                msg.channel.send({
                                    embeds: [
                                        exampleEmbed.setAuthor({ name: client.user.username + ` erreur...`, iconURL: client.user.displayAvatarURL(), url: ''})
                                        .setDescription(`L'utilisateur n'a pas été trouvé`)
                                    ]
                                })
                            }
                        }
                    break;
                    
                    case 'info':
                        addExperience(msg.author.id, 1)
                        msg.delete()

                        msg.channel.send({
                            embeds: [
                                exampleEmbed.setTitle('MrBob Informations')
                                .setDescription(':robot: Je suis un bot développé par Wookis#6378.\n:bulb: Si vous voulez me donner des idées faites : ```!idee [ MESSAGE ]```\n <:twitter:1079088903895650364> https://twitter.com/Wookkis')
                            ]
                        });
                    break;

                    case 'version':
                        addExperience(msg.author.id, 1)
                        msg.delete()

                        msg.channel.send({
                            embeds: [ 
                                exampleEmbed.setTitle('MrBob Version')
                                .setDescription('Je suis actuellement en ' + fs.readFileSync('./version.txt', 'utf8')) 
                            ]
                        });
                    break;

                    case 'update':
                        addExperience(msg.author.id, 2)
                        // Par défaut affiche la dernière page => dernière MAJ
                        if (args[1] === undefined || args[1] === null || args[1] === '') {
                            fs.readdir('./versionning', (err, files) => {
                                nFiles = files.length
    
                                msg.channel.send({
                                    embeds: [ 
                                    exampleEmbed.setTitle('MrBob Updates')
                                        .setDescription(fs.readFileSync(`./versionning/updates_${nFiles}.txt`, 'utf8')) 
                                        .setFooter({ text: `Page ${nFiles}/${nFiles}`, iconURL: client.user.displayAvatarURL() })
                                    ]
                                })    
                            });          
                        }
                        else {
                            const folderPath = './versionning/';
                            let nFiles = [];
                            x = 0
                            
                            fs.readdirSync(folderPath).forEach(file => {
                                x++
                                nFiles.push({ total: x});
                            });
                            
                            if ((args[1] < 1) || (args[1] > nFiles.length)) {
                                msg.channel.send({
                                    embeds: [ 
                                        exampleEmbed.setTitle('MrBob Updates')
                                        .setDescription(`Les pages sont comprises en 1 et ${nFiles.length}`) 
                                    ]
                                });
                            }
                            else {
                                msg.channel.send({
                                    embeds: [ 
                                    exampleEmbed.setTitle('MrBob Updates')
                                        .setDescription(fs.readFileSync(`./versionning/updates_${args[1]}.txt`, 'utf8')) 
                                        .setFooter({ text: `Page ${args[1]}/${nFiles.length}`, iconURL: client.user.displayAvatarURL() })
                                    ]
                                })    
                            }
                        }        
                    break;
                }
            }
            catch(err){
                logErreur(`Error : ${err.name} - ${err.message}`)
            }
        }
    }
});

client.login(auth.token);